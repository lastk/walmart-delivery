#WALMART webservice

##Exercicio

O Walmart esta desenvolvendo um novo sistema de logistica e sua ajuda é muito importante neste momento.
Sua tarefa será desenvolver o novo sistema de entregas visando sempre o menor custo. 
Para popular sua base de dados o sistema precisa expor um Webservices que aceite o formato de malha logística (exemplo abaixo),
nesta mesma requisição o requisitante deverá informar um nome para este mapa.
É importante que os mapas sejam persistidos para evitar que a cada novo deploy todas as informações desapareçam.
O formato de malha logística é bastante simples, cada linha mostra uma rota: ponto de origem,
ponto de destino e distância entre os pontos em quilômetros.

--------------------------------------------------------------------------------------------------------------------------------------------
A B 10

B D 15

A C 20

C D 30

B E 50

D E 30

----------------------------------------------------------------------------------------------------------------------------------------------

Com os mapas carregados o requisitante irá procurar o menor valor de entrega e seu caminho,
para isso ele passará o mapa, nome do ponto de origem, nome do ponto de destino, autonomia do
caminhão (km/l) e o valor do litro do combustivel, agora sua tarefa é criar este Webservices.
Um exemplo de entrada seria, mapa SP, origem A, destino D, autonomia 10, valor do litro 2,50; 
a resposta seria a rota A B D com custo de 6,25.

##Como Configurar o projeto

- Clone o projeto com git:

 - ` git clone git@bitbucket.org:lastk/walmart-delivery.git `

- Instale as dependências com o bundle:
  - `cd walmart-delivery `
  - ` bundle install `

- Crie o banco de dados ( Por padrão o banco configurado é o sqlite3 ):
  - ` rake db:create `
  - ` rake db:migrate `

- Rode os testes e tenha certeza de que tudo está funcionando da forma correta:
   - ` rspec spec/ `

##Como utilizar o webservice
Após seguir os passos anteriores e todos os testes passarem ( se algum teste não estiver passando, reveja os passos anteriores cuidadosamente ). Você pode rodar a aplicação com: 

  - ` rails s ` de dentro da pasta do projeto.

### Adicionando mapas
Para adicionar uma rota, deve ser feita uma requisição do tipo POST na url: `/api/routes` passando os seguintes parâmetros:

  - `map` : nome do mapa, exemplo: New York
  - `routes[origin]`: Nome do ponto de origem.
  - `routes[destination]`: Nome do ponto de destino
  - `routes[distance]`: Distância entre pontos em Km.

Se tudo ocorrer de acordo, você receberá uma resposta com status 200 (ok) indicando que foi 

Exemplo de criação utilizando o aplicativo `curl` com o web service rodando local:

`curl -v -X POST -d "map=testmapa" -d "routes[origin]=someplace" -d "routes[destination]=otherplace" -d "routes[distance]=10" http://localhost:3000/api/routes `


### Consultando rotas
Para consultar uma rota( previamente criada ) basta fazer uma consulta na url: `/api/itinerary` passando os seguintes parâmetros:

   - `map`: nome do mapa
   - `origin`: ponto de origem de dentro do mapa
   - `destination`: ponto de destino de dentro do mapa
   - `efficiency`: consumo de combustível do automóvel por KM
   -  `gas_price`: valor do combustível

Exemplo de uma consulta utilizando o aplicativo `curl` com o web service rodando local:

`  curl -v -X GET -d "map=testmapa&origin=someplace&destination=otherplace&efficiency=10&gas_price=20" http://localhost:3000/api/itinerary `
