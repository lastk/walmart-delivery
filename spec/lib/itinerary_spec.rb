require 'rails_helper'
describe Itinerary do
  fixtures :maps
  fixtures :routes

  context 'valid data' do
    let(:map) { maps(:map_test) }
    subject(:itinerary) { Itinerary.new(map.routes) }
    it 'returns the right path' do
      expect(itinerary.better_route('A','D')).to match_array(['A', 'B', 'D'])
    end

    it 'returns the right distance' do
      expect(itinerary.distance_of_better_route('A', 'D')).to eq(25.0)
    end
  end

  context 'invalid data' do
    let(:map) { maps(:map_test) }
    subject(:itinerary) { Itinerary.new(map.routes) }

    it 'returns false if the graph does not include a origin or destination' do
      expect(itinerary.include?('IDontExist')).to be_falsey
    end

    it 'returns a empty itinerary if it does not exists' do
      expect(itinerary.better_route('A','Heaven')).to be_empty
    end

    it 'returns -1 if the destination or origin does not exists' do
      expect(itinerary.distance_of_better_route('A','Heaven')).to eq(-1)
    end
  end
end
