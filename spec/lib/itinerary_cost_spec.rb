describe ItineraryCost do
  it 'calculates the cost of itinerary' do
    itinerary_cost = ItineraryCost.new(10,1, 22.6)
    expect(itinerary_cost.calculate).to eq(226.0)
  end

  context 'passing 0 for efficiency or gas price' do
    it 'returns zero if gas is 0' do
      itinerary_cost = ItineraryCost.new(20, 0,2)
      expect(itinerary_cost.calculate).to eq(0.0)
    end

    it 'returns zero if efficiency is 0' do
      itinerary_cost = ItineraryCost.new(20, 20,0)
      expect(itinerary_cost.calculate).to eq(0.0)
    end
  end
end
