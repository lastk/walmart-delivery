require 'rails_helper'
describe Dijkstra::Graph do
  subject(:graph) { Dijkstra::Graph.new }
  before do
    (1..6).each {|node| graph.push node }
    graph.connect_mutually 1, 2, 7
    graph.connect_mutually 1, 3, 9
    graph.connect_mutually 1, 6, 14
    graph.connect_mutually 2, 3, 10
    graph.connect_mutually 2, 4, 15
    graph.connect_mutually 3, 4, 11
    graph.connect_mutually 3, 6, 2
    graph.connect_mutually 4, 5, 6
    graph.connect_mutually 5, 6, 9
  end

  context 'Class methods' do
    describe '#length_between' do
      it 'returns the lenght between rotes' do
        expect(graph.length_between(2,1)).to eq(7)
        expect(graph.length_between(3,1)).to eq(9)
      end
    end

    describe '#neighbors' do
      it 'returns neighbors' do
        expect(graph.neighbors(2)).to match_array([1, 3, 4])
      end
    end

    describe '#dijkstra' do
      let(:result) { graph.dijkstra(1,6) }

      it 'contains the distance' do
        expect(result).to include(:distance)
      end

      it 'contains the path' do
        expect(result).to include(:path)
      end

      it 'returns the path' do
        expect(result[:path]).to match_array([1, 3, 6])
      end

      it 'returs the distance' do
        expect(result[:distance]).to eq(11)
      end
    end
  end
end
