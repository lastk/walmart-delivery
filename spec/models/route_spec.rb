require 'rails_helper'

describe Route do
  fixtures :routes

  context 'validations' do
    context 'invalid' do
      subject(:route) { Route.new }

      fields = %w( origin destination distance)

      fields.each do |attr|
        it "does not accept the field #{attr} in blank" do
          expect(route).to have(1).error_on(attr)
        end
      end

      it 'has a error when distance is < 0' do
        route = Route.new distance: -1
        expect(route).to have(1).error_on(:distance)
      end
    end

    context 'valid' do
      it 'is valid when all fields are filled' do
        wall_street_route = routes(:wall_street)

        expect(wall_street_route).to be_valid
      end

    end
  end
end
