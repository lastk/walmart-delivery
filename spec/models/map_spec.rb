require 'rails_helper'

describe Map do
  fixtures :maps

  context 'validations' do
    subject(:map) { Map.new }
    let(:gothan_city) { maps(:gothan_city) }

    context 'invalid' do
      it 'is invalid when name is blank' do
        expect(map).to be_invalid
      end

      it 'has a error on name when it is blank' do
        expect(map).to have(1).error_on(:name)
      end

      it 'is invalid when creating with the same name' do
        map.name = gothan_city.name
        expect(map).to have(1).error_on(:name)
      end
    end

    context 'valid' do
      let(:map) { Map.new( name: 'New york') }
      it 'is valid when it have a name' do
        expect(map).to be_valid
      end
    end
  end
end
