require 'rails_helper'

describe Api::RoutesController do
  describe 'POST create' do
    context 'valid data' do
      context 'new map' do
        let(:map_parameter) { 'Silent hill' }
        let(:route_parameter){
          [{origin: 'A',
             destination: 'B',
             distance: 192.3},
           {origin: 'A',
             destination: 'C',
             distance: 192.3}]
        }

        it 'returns a status ok' do
          post :create, { map: map_parameter, routes: route_parameter }

          expect(response).to have_http_status(:ok)
        end

        it 'saves map record to the database' do
          expect do
            post :create, { map: map_parameter, routes: route_parameter }
          end.to change(Map, :count).by(1)
        end

        it 'saves route record to the database' do
          expect do
            post :create, { map: map_parameter, routes: route_parameter }
          end.to change(Route, :count).by(2)
        end

      end

    end

    context 'invalid data' do
      let(:map_parameter) { 'New york' }
      let(:route_parameter) {
        { origin: 'A',
          destination: 'B',
          distance: 'C'
        }
      }

      it 'returns a bad request status' do
        post :create, { map: '', routes: route_parameter }
        expect(response).to have_http_status(:bad_request)
      end

      it 'does not create a record in database' do
        expect do
          post :create, { map: '', routes: route_parameter }
        end.to_not change(Map, :count)
      end
    end
  end

end
