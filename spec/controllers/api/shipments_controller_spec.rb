require 'rails_helper'

describe Api::ShipmentsController do
  fixtures :maps
  fixtures :routes

  describe 'GET shipment' do
    context 'with valid data' do
      let(:parameters) {
        {map: 'route_66', origin: 'A', destination: 'D', efficiency: '2', gas_price: '5'}
      }

      before do
        get :itinerary, parameters
      end

      it 'returns http success' do
        expect(response).to have_http_status(:ok)
      end

      it 'returns the distance' do
        body = JSON.parse(response.body)
        expect(body['distance']).to eq('25.0')
      end

      it 'returns the cost of the itinerary' do
        body = JSON.parse(response.body)
        expect(body['route']).to match_array(["A", "B", "D"])
      end
    end

    context 'with invalid data' do
      it 'returns precondition failed error status' do
        get :itinerary
        expect(response).to have_http_status(:precondition_failed)
      end

      context 'itinerary without route' do
        let(:parameters) {
          {
            map: 'route_66',
            origin: 'Heaven',
            destination: 'B',
            efficiency: '10',
            gas_price: '5'
          }
        }

        it 'returns a pre condition required status error' do
          get :itinerary, parameters
          expect(response). to have_http_status(:precondition_required)
        end
      end
    end
  end

end
