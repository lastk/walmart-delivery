class ItineraryCost
  def initialize(distance, efficiency, gas_price)
    @distance = distance
    @efficiency = efficiency
    @gas_price = gas_price
  end

  def calculate
    return 0.0 if @efficiency == 0.0 or @gas_price == 0.0

    (@distance / @efficiency) * @gas_price
  end
end
