class ItineraryJson
  def initialize(route, distance, cost)
    @route = route
    @distance = distance
    @cost = cost
  end
end
