class Itinerary
  def initialize(routes)
    @graph = Dijkstra::Graph.new
    initialize_graph(routes)
  end

  def to_json(origin, destination, efficiency, gas_price)

    efficiency = sanitize_numeric_parameter(efficiency)
    gas_price  = sanitize_numeric_parameter(gas_price)

    calculated = calculate_route(origin, destination)
    cost = ItineraryCost.new(calculated[:distance], efficiency, gas_price).calculate
    return ItineraryJson.new(calculated[:path], calculated[:distance], cost).to_json
  end

  def better_route(origin, destination)
    route = calculate_route(origin, destination)
    if route.present?
      route[:path]
    else
      []
    end
  end

  def distance_of_better_route(origin, destination)
    route = calculate_route(origin, destination)
    if route.present?
      route[:distance]
    else
      -1
    end
  end

  def include?(origin_or_destination)
    @graph.include?(origin_or_destination)
  end

  private

  def calculate_route(origin, destination)
     @graph.dijkstra(origin, destination)
  end

  def sanitize_numeric_parameter(parameter)
    parameter.to_f
  end

  def initialize_graph(routes)
    push_localizations(routes)
    routes.each do |route|
      @graph.connect_mutually route.origin, route.destination, route.distance
    end
  end

  def push_localizations(routes)
    routes.each do |route|
      @graph.push route.origin unless @graph.include?(route.origin)
      @graph.push route.destination unless @graph.include?(route.destination)
    end
  end
end
