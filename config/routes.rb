Rails.application.routes.draw do
  namespace :api do
    resource :routes, only: [:create]
    get 'itinerary', to: 'shipments#itinerary'
  end
end
