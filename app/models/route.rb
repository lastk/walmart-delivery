class Route < ActiveRecord::Base
  belongs_to :map
  validates :origin, :destination, :distance, presence: true
  validates :distance, numericality: { greater_than: 0 }, if: 'distance.present?'
end
