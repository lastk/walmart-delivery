class Api::RoutesController < ApiController
  rescue_from ActiveRecord::RecordInvalid,
              with: :record_invalid

  def create
    current_map.routes.build routes_parameters
    current_map.save!
    render json: current_map.to_json, status: :ok
  end

  private

  def current_map
    @map ||= Map.find_or_initialize_by(name: map_parameter)
  end

  def record_invalid(error)
    render_error(:bad_request, error.message)
  end

  def routes_parameters
    params.permit(routes: [:origin, :destination, :distance])[:routes]
  end

  def map_parameter
    params[:map]
  end

end
