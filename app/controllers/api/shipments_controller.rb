class Api::ShipmentsController < ApiController
  before_action :filter_params
  before_action :find_map

  rescue_from ActiveRecord::RecordNotFound, with: :not_found_error

  def itinerary
    @itinerary = Itinerary.new(@map.routes)
    unless verify_routes
      render_error(:precondition_required, 'You must include all your routes before searching') and return
    end
    render json: @itinerary.to_json(params[:origin], params[:destination], params[:efficiency], params[:gas_price]), status: :ok
  end

  private

  def find_map
    @map ||= Map.find_by!(name: params[:map])
  end

  def verify_routes
    (@itinerary.include?(params[:origin]) and @itinerary.include?(params[:destination]))
  end

  def filter_params
    return argument_error("You must send all parameters specified. Parameters missing: #{missing_parameters.join(',')}") if missing_parameters.any?
  end

  def parameters
    [:map, :origin, :destination, :efficiency, :gas_price]
  end

  def not_found_error
    render_error(:not_found, 'Map not found')
  end

  def argument_error(error)
    render_error(:precondition_failed, error)
  end

  def missing_parameters()
    parameters.map do |p|
      p unless params.include?(p)
    end
  end
end
