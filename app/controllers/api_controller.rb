class ApiController < ActionController::Base
  protect_from_forgery with: :null_session

  def render_error(status, message)
    render json: message, status: status
  end
end
